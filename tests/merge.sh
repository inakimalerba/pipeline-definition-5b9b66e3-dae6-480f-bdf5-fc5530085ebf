#!/bin/bash
set -euo pipefail

for test_script in merge/*.sh; do
  tests/${test_script}
done
